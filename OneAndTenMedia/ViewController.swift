//
//  ViewController.swift
//  OneAndTenMedia
//
//  Created by macbookair-gm on 18/01/20.
//  Copyright © 2020 macbookair-gm. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var moviesTableView: UITableView!
    var arrayOfMovies : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        moviesTableView.delegate = self
        moviesTableView.dataSource = self
        
        getData()
    }
    
    func getData(){
        let session = URLSession.shared
        let url = URL(string: "https://api.themoviedb.org/3/movie/now_playing?api_key=1d9b898a212ea52e283351e521e17871&amp;language=en&amp;page=2&amp;region=US")!
        
        let task = session.dataTask(with: url) { data, response, error in
            
            if error != nil || data == nil {
                print("Client error!")
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error!")
                return
            }
            
            guard let mime = response.mimeType, mime == "application/json" else {
                print("Wrong MIME type!")
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                
                self.arrayOfMovies.addObjects(from: json?["results"] as! [Any])
                print(self.arrayOfMovies.count)
                DispatchQueue.main.async {
                    self.moviesTableView.reloadData()
                }
                
            } catch {
                print("JSON error: \(error.localizedDescription)")
            }
        }
        
        task.resume()
    }
}

extension ViewController : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfMovies.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieTableViewCell") as! MovieTableViewCell
        
        let dictionary = arrayOfMovies.object(at: indexPath.row) as! NSDictionary 
        
        
        cell.setData(datavalue: dictionary)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

