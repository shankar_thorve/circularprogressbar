//
//  movieTableViewCell.swift
//  OneAndTenMedia
//
//  Created by macbookair-gm on 18/01/20.
//  Copyright © 2020 macbookair-gm. All rights reserved.
//

import UIKit
import SDWebImage

class MovieTableViewCell: UITableViewCell {
    @IBOutlet weak var lblOverview: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var progressBar: CircularProgressView!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var imgPoster: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
        //progressBar.setProgressWithAnimation(duration: 0, value: 0)
         
        imgPoster.layer.cornerRadius = 4;
        imgPoster.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(datavalue:NSDictionary) {
        lblTitle.text = datavalue.value(forKey: "title") as? String
        lblOverview.text = datavalue.value(forKey: "overview") as? String
           
        lblDate.text = datavalue.value(forKey: "release_date") as? String
        
        let imgurl = "https://image.tmdb.org/t/p/w300_and_h450_bestv2" + ( datavalue.value(forKey: "poster_path") as! String)
        
        imgPoster.sd_setImage(with: URL(string: imgurl), placeholderImage: UIImage(named: "placeholder.png"))
        
        if let progressCount = datavalue.value(forKey: "vote_average") as? Float{
           
            progressBar.trackClr = UIColor.gray
            progressBar.progressClr = UIColor(red: 251/255.0, green: 204/255.0, blue: 51/255.0, alpha: 1.0)
            progressBar.makeCircularPath()
                
            progressBar.setProgressWithAnimation(duration: 0.1, value: (progressCount*10)/100)
                lblPercentage.text = "\((Int)(progressCount * 10))%"
            
        }
        
    }

}
